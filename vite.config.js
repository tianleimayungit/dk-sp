import {
	fileURLToPath,
	URL
} from 'node:url'

import {
	defineConfig
} from 'vite'
import vue from '@vitejs/plugin-vue'
import postCssPxToRem from 'postcss-pxtorem'

// https://vitejs.dev/config/
export default defineConfig({
	server: {
		proxy: {
		  '/api': {
			target: 'http://104.233.167.26:6688',
			changeOrigin: true,
			rewrite: path => path.replace(/^\/api/, '')
		  }
		}
	  },
	plugins: [
		vue(),
	],
	resolve: {
		alias: {
			'@': fileURLToPath(new URL('./src', import.meta.url))
		}
	},
	base:'./',
	css: {
		postcss: {
			plugins: [
				postCssPxToRem({
					rootValue: 37.5,
					propList: ['*'],
					unitPrecision: 3,
				})
			]
		}
	}

})