import {
	createVNode,
	render
} from 'vue'

import Template from './index.vue';



const Alert = (msg, btn, e = function(){}, close = false) => {
	const container = document.createElement('div')
	const vm = createVNode(Template, {
		value: msg,
		btn: btn,
		close:close
	}) // 创建vNode
	render(vm, container)
	document.body.appendChild(container) // 添加到body上
	const dom = vm.el;
	const box = dom.querySelector(".dialog-body");
	if (box) {
		box.classList.add('active');
	}

	const btnDom = close ? dom.querySelector(".dialog-action-row") :dom.querySelector(".dialog-footer");
	
	return btnDom.addEventListener("click", function (item){
		// 判断 按下的是谁
		box.classList.remove("active");
		box.classList.add("scaledown");
		setTimeout(() => {
			render(null, container);
			document.body.removeChild(container);
			if ((close && item.target.textContent == btn) || !close) {
				e();
			}
			
		}, 300)
	});
	
	
}

export default Alert