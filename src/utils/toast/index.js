import {
	createVNode,
	render
} from 'vue'

import toastTemplate from './toast.vue';

const Toast = (msg) => {
	const container = document.createElement('div')
	const vm = createVNode(toastTemplate, {
		value: msg
	}) // 创建vNode
	render(vm, container)
	document.body.appendChild(container) // 添加到body上
	const dom = vm.el;
	dom.classList.add('active')
	const destory = () => {
		dom.classList.remove('active')
		const t = setTimeout(() => { // 淡入淡出效果之后删除dom节点
			render(null, container)
			document.body.removeChild(container)
			clearTimeout(t)
		}, 300);
	}

	const timer = setTimeout(() => {
		destory();
		clearTimeout(timer)
	}, 1500)

	return {
		destory
	}
}

export default Toast