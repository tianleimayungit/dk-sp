const state = {
    personal: false,
}
const mutations = {
    SET_USER_INFO(state, { key, value }) {
        state[key] = value
    }
}
const actions = {
    setUserInfo({ commit }, { key, value }) {
        commit('SET_USER_INFO', { key, value })
    }

}
export default {
    namespaced: true,
    state,
    actions,
    mutations
}