import {
	createStore
} from 'vuex';
import user from './user';
import AJAX from '@/utils/http';
//状态管理
export default createStore({
	modules: {
		user
	},
	state: {
		isLoading: false,
		kefu_link: '',
	},
	getters:{
		userInfo: state => state.user,
		kefu_link:state => state.kefu_link
	},
	mutations: {
		SET_KEFU_LINK(state,url){
			state.kefu_link = url;
		},
		showLoading(state) {
			state.isLoading = true;
		},
		hideLoading(state) {
			state.isLoading = false;
		}
	},
	actions:{
		getAppInfo({commit}){
			return new Promise(async (resolve,reject) => {
				const response = await AJAX.post('/index/index', {})
				commit('SET_KEFU_LINK',response.data.kefu_link);
				resolve();
			})
		},
		getKf(){
			return new Promise(async (resolve) => {
				const response = await AJAX.post('/index/index', {})
				const a = document.createElement('a');
				a.href = response.data.kefu_link;
				a.target = "_blank";
				a.click();
				resolve();
			})
		},
	}
});