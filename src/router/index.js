// 配置路由
import {
	createRouter,
	createWebHashHistory
} from 'vue-router'
import store from '@/store/index'
import Home from '../view/index/index.vue';
import Login from '../view/user/login.vue';
import NewLogin from '../view/user/newLogin.vue';
import Register from '../view/user/register.vue';
const noNeedLogin = ['Register', 'Login','NewLogin'];
const routes = [
	{
		path: '/',
		component: Home,
	},
	{
		path: '/newLogin',
		component: NewLogin,
		name:'NewLogin',
	},
	{
		path: '/login',
		component: Login,
		name:'Login',
	},
	{
		path: '/register',
		component: Register,
		name:'Register',
	},
	{
		path: '/user/personal',
		component: () => import("../view/user/personal.vue"),
	},
	{
		path: '/user/work',
		component: () => import("../view/user/work.vue"),
	},
	{
		path: '/user/bank',
		component: () => import("../view/user/bank.vue"),
	},
	{
		path: '/user/index',
		component: () => import("../view/user/index.vue"),
	},
	{
		path: '/user/drawing',
		component: () => import("../view/user/drawing.vue"),
	},
	{
		path: '/index/loan',
		component: () => import("../view/index/loan.vue"),
	},
	{
		path: '/index/aggrement',
		component: () => import("../view/index/aggrement.vue"),
	},

	{
		path: '/index/esign',
		component: () => import("../view/index/esign.vue"),
	},
	{
		path: '/index/pay',
		component: () => import("../view/index/pay.vue"),
	},
];

const router = createRouter({
	history: createWebHashHistory(),
	routes: routes
});


// GOOD
router.beforeEach((to, from, next) => {
	let isAuthenticated = localStorage.getItem("token");
	if(to.query.invite != undefined){
		localStorage.setItem('invite',to.query.invite);
	}


	if (!noNeedLogin.includes(to.name) && !isAuthenticated) {
		next({
			name: 'NewLogin'
		});
	} else {
		store.dispatch('getAppInfo').then(() => {
			window.scrollTo(0,0);
			next();
		})
	}
});

export default router
