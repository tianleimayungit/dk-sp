import './assets/main.css'
import './assets/iconfont.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'amfe-flexible'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
//导入状态管理
// import Store from '@/store/index.js';

import Toast from '@/utils/toast/index.js'; // 导入 Toast 插件
import Alert from '@/utils/alert/index.js'; // 导入 Toast 插件
import Store from '@/store/index.js';
var app = createApp(App);
app.config.globalProperties.$toast = Toast;
app.config.globalProperties.$alert = Alert;
app.use(router).use(Store).use(ElementPlus).mount('#app');

